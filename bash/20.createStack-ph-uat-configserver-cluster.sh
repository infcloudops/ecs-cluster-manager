aws cloudformation create-stack --stack-name ph-stg-configserver-cluster --template-body file://./ecs-cluster.yaml \
--parameters file://./ph-stg-configserver-cluster.json  \
  --region ap-southeast-1
