aws cloudformation create-stack --stack-name ph-stg-zipkin-cluster --template-body file://./ecs-cluster.yaml \
--parameters file://./ph-stg-zipkin-service-cluster.json  \
  --region ap-southeast-1
